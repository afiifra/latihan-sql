create database tokoku;
use tokoku;

create table users(
id int primary key auto_increment,
name varchar(255),
email	varchar(255),
password	varchar(255)
);

create table categories(
id int primary key auto_increment,
name varchar(255)
);

create table items(
id int primary key auto_increment,
name varchar(255),
description	varchar(255),
price	integer,
stock	integer,
category_id	integer,
foreign key(category_id) references categories(id)
);

insert into users(name,	email, password) 
values("John Doe",	"john@doe.com",	"john123"),
	  ("Jane Doe",  "jane@doe.com",	"jenita123");

insert into categories(name)
values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

insert into items (name, description, price, stock,	category_id)
values ("Sumsang b50",	"hape keren dari merek sumsang", 4000000	,100, 1),
	   ("Uniklooh",	"baju keren dari brand ternama", 500000,	50,	2),
	   ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000,	10,	1)
       
	select name, email from users;
    
    select price from items where price >1000000;
    
    select items.name, items.description, items.price, items.stock, items.category_id,
    items.category_id, categories.id inner join on 
    items.category_id = categories.id;
       


